FROM openjdk:8-jre-alpine

COPY home-credit-exam-2-0.0.1-SNAPSHOT.jar /app.jar

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom", "-jar", "-Dspring.profiles.active=default", "/app.jar"]