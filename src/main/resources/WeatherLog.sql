CREATE TABLE `home_credit_exam`.`WeatherLog` (
  `Id` BIGINT NOT NULL AUTO_INCREMENT,
  `responseId` VARCHAR(255) NULL,
  `location` VARCHAR(45) NULL,
  `actualWeather` VARCHAR(45) NULL,
  `temperature` VARCHAR(45) NULL,
  `dtimeInserted` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`));