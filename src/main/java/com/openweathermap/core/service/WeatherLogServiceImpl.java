package com.openweathermap.core.service;

import com.openweathermap.infrastracture.entity.WeatherLogEntity;
import com.openweathermap.infrastracture.repository.WeatherLogRepository;
import com.openweathermap.presentation.data.CityWeatherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherLogServiceImpl implements WeatherLogService{
    private final WeatherLogRepository weatherLogRepository;

    @Autowired
    public WeatherLogServiceImpl(WeatherLogRepository weatherLogRepository) {
        this.weatherLogRepository = weatherLogRepository;
    }

    @Override
    public void saveResponse(String responseId, List<CityWeatherDTO> cityWeatherDTOList){
        List<WeatherLogEntity> weatherLogEntityListForSave = new ArrayList<>();
        System.out.println("test2");
//        System.out.println();

        cityWeatherDTOList.forEach(cityWeatherDTO -> weatherLogEntityListForSave.add(new WeatherLogEntity(cityWeatherDTO, responseId)));

        List<WeatherLogEntity> weatherLogEntityList = weatherLogRepository.findAll();

        // Not quite sure about the requirement "store last five unique responses of API #1". So what I did is to store only the last five unique responses and remove old ones
        // Since one unique response from API #1 is stored in 3 entries for each city. 15 entries means 5 unique responses.
        if (weatherLogEntityList.size() >= 15) {
            weatherLogEntityList.stream().limit(3).forEach(weatherLogRepository::delete);
        }

//        weatherLogRepository.save(weatherLogEntityListForSave);
        weatherLogRepository.saveAll(weatherLogEntityListForSave);
    }
}