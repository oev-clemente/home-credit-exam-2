package com.openweathermap.core.service;

import com.openweathermap.presentation.data.CityWeatherDTO;

import java.util.List;

public interface WeatherLogService {

    void saveResponse(String responseId, List<CityWeatherDTO> cityWeatherDTOList);
}
