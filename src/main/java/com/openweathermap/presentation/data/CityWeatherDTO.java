package com.openweathermap.presentation.data;

import java.io.Serializable;

public class CityWeatherDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5770327882288577996L;
	private String test;
	private String location;
	private String actualWeather;
	private String temperature;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getActualWeather() {
		return actualWeather;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
}
