package com.openweathermap.presentation.controller;

import com.openweathermap.core.service.WeatherLogServiceImpl;
import com.openweathermap.presentation.data.CityWeatherDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@Slf4j
@RestController
@RequestMapping("/weather")
public class CityWeatherController {

	private final WeatherLogServiceImpl weatherLogService;

	@Autowired
	public CityWeatherController(WeatherLogServiceImpl weatherLogService) {
		this.weatherLogService = weatherLogService;
	}

	@PostMapping("/saveresponse")
	public ResponseEntity<String> saveresponse(@RequestHeader HttpHeaders httpheaders, @RequestBody List<CityWeatherDTO> cityWeatherDTOList) {
		String responseId = Objects.requireNonNull(httpheaders.get("GUID")).stream().findFirst().orElse("");

		log.info("test: {}", responseId);
		weatherLogService.saveResponse(responseId, cityWeatherDTOList);
		return new ResponseEntity<>("OK111243", HttpStatus.CREATED);
	}

}
