package com.openweathermap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeCreditExam2Application {

	public static void main(String[] args) {
		SpringApplication.run(HomeCreditExam2Application.class, args);
	}
}
