package com.openweathermap.infrastracture.repository;

import com.openweathermap.infrastracture.entity.WeatherLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherLogRepository extends JpaRepository<WeatherLogEntity, Integer> {

}
