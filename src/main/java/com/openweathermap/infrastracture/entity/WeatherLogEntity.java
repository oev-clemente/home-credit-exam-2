package com.openweathermap.infrastracture.entity;

import com.openweathermap.presentation.data.CityWeatherDTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "WeatherLogEntity")
public class WeatherLogEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String responseId;
	private String location;
	private String actualWeather;
	private String temperature;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dtimeInserted", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
	private Date dtimeInserted = new Date();

	public WeatherLogEntity() {

	}

	public WeatherLogEntity(CityWeatherDTO cityWeatherDTO, String responseId) {
		this.setActualWeather(cityWeatherDTO.getActualWeather());
		this.setLocation(cityWeatherDTO.getLocation());
		this.setTemperature(cityWeatherDTO.getTemperature());
		this.setResponseId(responseId);
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getActualWeather() {
		return actualWeather;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public Date getDtimeInserted() {
		return dtimeInserted;
	}

	public void setDtimeInserted(Date dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}

	public Long getId() {
		return id;
	}

}
